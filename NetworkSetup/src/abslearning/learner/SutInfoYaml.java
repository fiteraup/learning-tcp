package abslearning.learner;

import java.util.List;
import java.util.Map;

public class SutInfoYaml {
	public List<Integer> constants;
	public Map<String, List<String>> inputInterfaces;
	public Map<String, List<String>> outputInterfaces;
	public String name;
}

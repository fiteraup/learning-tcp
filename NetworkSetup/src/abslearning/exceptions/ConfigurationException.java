package abslearning.exceptions;

public class ConfigurationException extends RuntimeException{
	private static final long serialVersionUID = 3349908291398696155L;

	public ConfigurationException(String message) {
		super(message);
	}
}

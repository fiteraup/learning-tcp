LEARNDIR=NetworkSetup
LIBDIR=lib
INPUTDIR=input
BINDIR=bin
cd $LEARNDIR
CLASSPATH=$LIBDIR/*:$BINDIR
echo $CLASSPATH
java -cp $CLASSPATH abslearning.learner.Main --port $1 $INPUTDIR/tcp/config.yaml
cd ..

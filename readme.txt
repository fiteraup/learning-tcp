The repository was moved from bitbucket to google code. I opted for this change since google code allows you to have (at least in appearance) a page for your project. 
The link of the new repo is: https://code.google.com/p/tcp-learner/ 

BitBucket is still great and I will continue to use it. :) 
I will try to keep this updated but I cannot guarantee it.
